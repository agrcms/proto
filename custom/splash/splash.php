<?php
  $server_name = $_SERVER['SERVER_NAME'];
  $host = $_SERVER['HTTP_HOST'];
  $prod = FALSE;
  if (stripos($host, 'wip') > 0 || stripos($host, 'grisource.agr.gc.ca') > 0) {
    $prod = TRUE;
  }
  // Adobe Analytics JS library.
  $adobetm='//assets.adobedtm.com/caacec67651710193d2331efef325107c23a0145/satelliteLib-c2082deaf69c358c641c5eb20f94b615dd606662-staging.js';
  if ($prod) {
    $adobetm='//assets.adobedtm.com/caacec67651710193d2331efef325107c23a0145/satelliteLib-c2082deaf69c358c641c5eb20f94b615dd606662.js';
  }
?>
<!DOCTYPE html><!--[if lt IE 8]><html class="no-js lt-ie9" lang="en" dir="ltr"><![endif]--><!--[if gt IE 7]><!-->
<html class="no-js" lang="en" dir="ltr">
<!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8">
<title>Language selection - AgriSource / Sélection de la langue - AgriSource</title>
<meta content="width=device-width,initial-scale=1" name="viewport">
<meta name="description" content="Splash page">
<meta property="description" lang="fr" content="Page d'entrée">
<meta property="dcterms.creator" content="Agriculture and Agri-Food Canada">
<meta property="dcterms.creator" lang="fr" content="Agriculture et Agroalimentaire Canada">
<meta property="dcterms.title" content="AgriSource">
<meta property="dcterms.title" lang="fr" content="AgriSource">
<meta property="dcterms.issued" title="W3CDTF" content="2016-12-13">
<meta property="dcterms.modified" title="W3CDTF" content="">
<meta property="dcterms.subject" title="scheme" content="Government of Canada, services">
<meta property="dcterms.subject" lang="fr" title="scheme" content="Gouvernement du Canada, services">
<meta property="dcterms.language" title="ISO639-2" content="eng">
<meta property="dcterms.language" lang="fr" title="ISO639-2" content="fra">
<meta name="robots" content="noindex, follow">
<meta property="dcterms:service" content="AAFC_AgriSource"/>
<meta property="dcterms.accessRights" content="3"/>
<!--[if gte IE 8 | !IE ]><!-->
<link href="/sites/default/splash/img/favicon.ico" rel="icon" type="image/x-icon">
<link rel="stylesheet" href="/sites/default/splash/css/theme.min.css">
<link rel="stylesheet" href="/sites/default/splash/css/splashtheme.css">
<!--<![endif]-->
<!--[if lt IE 8]>
<link href="/sites/default/splash/img/favicon.ico" rel="shortcut icon"/>
<link rel="stylesheet" href="/sites/default/splash/css/ie8-theme.min.css"/>
<script src="/sites/default/splash/js/jquery.min.js"></script>
<script src="/sites/default/splash/js/ie8-wet-boew.min.js"></script>
<![endif]-->
<noscript><link rel="stylesheet" href="/sites/default/splash/css/noscript.min.css"/></noscript>
<script src="<?php print $adobetm ?>"></script>
</head>
<body vocab="https://schema.org/" typeof="WebPage">
<header role="banner" id="wb-bnr">
<div class="container">
<div class="row mrgn-tp-lg mrgn-bttm-lg">
<div class="col-md-8 col-md-offset-2">
<object type="image/png" tabindex="-1" role="img" data="/sites/default/splash/img/aslogo.png" aria-label="AgriSource"></object>
</div>
</div>
</div>
</header>
<main role="main" property="mainContentOfPage" class="container">
<div class="row mrgn-tp-lg">
<div class="col-md-12">
<h1 class="wb-inv">Language selection - AgriSource / <span lang="fr">Sélection de la langue - AgriSource</span></h1>
<section class="col-md-6">
<h2 class="h3 text-center">AgriSource</h2>
<ul class="list-unstyled">
<li><a class="btn btn-lg btn-primary btn-block" href="/en/">English</a></li>
<li><a class="btn btn-lg btn-default btn-block mrgn-tp-sm" href="en/important-notices" rel="license">Terms and conditions of use</a></li>
</ul>
</section>
<section class="col-md-6" lang="fr">
<h2 class="h3 text-center">AgriSource</h2>
<ul class="list-unstyled">
<li><a class="btn btn-lg btn-primary btn-block" href="/fr/">Français</a></li>
<li><a class="btn btn-lg btn-default btn-block mrgn-tp-sm" href="fr/avis-importants" rel="license">Conditions régissant l'utilisation</a></li>
</ul>
</section>
</div>  
</div>
</main>
<!--[if gte IE 9 | !IE ]>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script src="/sites/default/splash/js/wet-boew.min.js"></script>
<!--<![endif]-->
<!--[if lt IE 9]-->
<script src="/sites/default/splash/js/ie8-wet-boew2.min.js"></script>

<!--[endif]-->
<script src="/sites/default/splash/js/theme.min.js"></script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>
